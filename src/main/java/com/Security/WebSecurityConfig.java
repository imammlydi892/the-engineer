package com.Security;

import com.services.AppUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
    
    @Autowired
    private AppUserService appUserService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


//MEMBERIKAN AKSES SECURITY KE ENDPOINT REGISTER SELAIN REGISTER HARUS LOGIN DULU
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/api/engineer").hasAuthority("ENGINEER")
        .antMatchers("/api/support").hasAuthority("SUPPORT")
        .antMatchers("/api/users/register").permitAll()
        .antMatchers("/api/login").permitAll()
        // .and()
        //     .formLogin(form - > form
        //         .loginPage("/login")
        //         .defaultSuccessUrl("/home")
        //         .failureUrl("/login?error=true")
        //     )
        .anyRequest().fullyAuthenticated()
        .and()
        .formLogin()
        .loginPage("/api/login")
        .defaultSuccessUrl("/home")
        // .and()
        // .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/api/login")
        // .failureUrl("/login-error.html")
        .and().httpBasic().disable();
    }


    


    //MENDAFTARKAN METHOD ENCODER KE SPRING
    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(bCryptPasswordEncoder);
        provider.setUserDetailsService(appUserService);

        return provider;
    }


    //DEKLARASI
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.authenticationProvider(daoAuthenticationProvider());
    }
}
