package com.utility;

import java.util.Optional;

import com.entities.AppUser;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwares implements AuditorAware<String> {


    // MEMBERI TAHU SIAPA USER YANG LOGIN
    @Override
    public Optional<String> getCurrentAuditor() {
        AppUser currentUser = (AppUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Optional.of(currentUser.getFullName());
    }
    
}
