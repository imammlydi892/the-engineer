package com.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.entities.ListReport;
import com.repo.ListReportRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ListReportService {
    
    @Autowired
    private ListReportRepo listReportRepo;

    public ListReport create(ListReport listReport){
        return listReportRepo.save(listReport);
    }

    public ListReport findOne(Long id){
        Optional<ListReport>  listreports= listReportRepo.findById(id);
        if(!listreports.isPresent()){
            return null;
        }
        return listreports.get();
    }

    public Iterable<ListReport> findAll(){
        return listReportRepo.findAll();
    }

    public void deleteById(Long id){
        listReportRepo.deleteById(id);
    }

    public List<ListReport> findByTitle(String keyword){
        return listReportRepo.findByTitleContains(keyword);
    }
}
