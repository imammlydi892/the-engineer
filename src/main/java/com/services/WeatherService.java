package com.services;

import java.util.Optional;

import javax.transaction.Transactional;

import com.entities.Weather;
import com.repo.WeatherRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class WeatherService {
    

    @Autowired
    private WeatherRepo weatherRepo;

    public Weather  create (Weather weather){
        return weatherRepo.save(weather);

    }

    public Iterable<Weather> findAll(){
        return weatherRepo.findAll();
    }

    public Weather findOne(Long id){
        Optional<Weather> weathers = weatherRepo.findById(id);
        if(!weathers.isPresent()){
            return null;
        }
        return weathers.get();
    }
}
