package com.services;


import java.util.Optional;

import javax.transaction.Transactional;

import com.entities.AppUser;
import com.entities.Engineer;
import com.entities.Support;
import com.repo.AppUserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AppUserService implements UserDetailsService {

    @Autowired
    private AppUserRepo appUserRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
      
        return appUserRepo.findByEmail(email)
            .orElseThrow(() -> 
            new UsernameNotFoundException(
                String.format("user with email '%s' not found", email)));
    }
    
    // register user
    public AppUser registerAppUser(AppUser appUser){
        boolean userExist = appUserRepo.findByEmail(appUser.getEmail()).isPresent();
        if(userExist){
            throw new RuntimeException(
                String.format("user with email '%s' already Exist ", appUser.getEmail())
            );
        }

        //encrypt password
        String encodedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(encodedPassword);
        return appUserRepo.save(appUser);

    }

    public AppUser findById(Long id){
        Optional<AppUser> user = appUserRepo.findById(id);
        if(!user.isPresent()){
            return null;
        }
        return user.get();
    }

    public Iterable<AppUser> findAll(){
        return appUserRepo.findAll();
    }

    public AppUser saveEng(AppUser appUser){
        return appUserRepo.save(appUser);
    }

    


    //JOIN COLUMN
    public void addEngineer(Engineer engineer, Long id){
        AppUser user = findById(id);
        if(user==null){
            throw new RuntimeException("User with Id = "+id+" is no there");
        }

        // user.getEngineer();
        engineer.setUser(user);
        user.setEngineer(engineer);
        saveEng(user);
    }

    public void addSupport(Support support, Long id){
        AppUser users = findById(id);
        if(users == null){
            throw new RuntimeException("User with Id = "+id+" is no there");
        }
        support.setUser(users);
        users.setSupport(support);
        saveEng(users);
    }
}
