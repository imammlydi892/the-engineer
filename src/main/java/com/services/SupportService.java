package com.services;

import java.util.Optional;

import com.entities.ReportSupport;
import com.entities.Support;
import com.repo.SupportRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SupportService {

    @Autowired
    private SupportRepo supportRepo;

    public Support create(Support support){
        return supportRepo.save(support);
    }

    public Iterable<Support> findAll(){
        return supportRepo.findAll();
    }

    public Support findOne(Long id){
        Optional<Support> supports = supportRepo.findById(id);
        if(!supports.isPresent()){
            return null;
        }
        return supports.get();
    }

    //join 
    public void addReport(ReportSupport reportSupport,Long id){
        Support support = findOne(id);
        if(support==null){
            throw new RuntimeException("Support with id "+id+"not found");
        }
        support.getReportSupports().add(reportSupport);
        create(support);
    }
}
