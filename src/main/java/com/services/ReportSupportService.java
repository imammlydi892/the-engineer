package com.services;

import java.util.Optional;

import javax.transaction.Transactional;

import com.entities.ListReport;
import com.entities.ReportSupport;
import com.entities.Weather;
import com.repo.ReportSupportRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ReportSupportService {

    @Autowired
    private ReportSupportRepo reportSupportRepo;
    
    public Iterable<ReportSupport> findAll(){
        return reportSupportRepo.findAll();
    }


    public ReportSupport finOne(Long id){
        Optional<ReportSupport> reportSupport = reportSupportRepo.findById(id);
        if(!reportSupport.isPresent()){
            return null;
        }
        return reportSupport.get();
    }

    public ReportSupport create(ReportSupport reportSupport){
        return reportSupportRepo.save(reportSupport);
    }

    public void addWeather(Weather weather, Long id){
        ReportSupport reportSupport = finOne(id);
        if(reportSupport==null){
            throw new RuntimeException("Weather with  "+id+" is null");
        }
        reportSupport.getWeathers().add(weather);
        create(reportSupport);
    }

    public void addListReport(ListReport listReport, Long id){
        ReportSupport reportSupports= finOne(id);
        if(reportSupports==null){
            throw new RuntimeException("ListReport with "+id+" is Null");
        }
        reportSupports.getListReport().add(listReport);
        create(reportSupports);
    }
}
