package com.services;

import java.util.Optional;
// import java.util.UUID;

import javax.transaction.Transactional;

import com.entities.Engineer;
import com.entities.Support;

import com.repo.EngineerRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EngineerService {
    
    @Autowired
    private EngineerRepo engineerRepo;

    // @Autowired
    // private Support support;


    public Iterable<Engineer> findAll(){
        return engineerRepo.findAll();
    }

    public Engineer  create(Engineer engineer){
        return engineerRepo.save(engineer);
    }

    public Engineer finOne(Long id){
        Optional<Engineer> engineer = engineerRepo.findById(id);
        if(!engineer.isPresent()){
            return null;
        }
        return engineer.get();
    }

//join
    public void addSupport(Support supports, Long id){
        Engineer engineer = finOne(id);
        if(engineer == null){
            throw new RuntimeException("engineer with id "+id+"not found");
        }
        engineer.getSupports().add(supports);
        create(engineer);
    }
    
}
