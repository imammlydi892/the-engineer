package com.controllers;

import com.dto.AppUserDto;
import com.dto.ResponseData;
import com.entities.AppUser;
import com.entities.Engineer;
import com.entities.Support;
import com.services.AppUserService;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/users")
public class AppUserController {
    

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/register")
    public ResponseEntity<ResponseData<AppUser>> register(@RequestBody AppUserDto appUserDto){

        ResponseData<AppUser> response= new ResponseData<>();
        AppUser appUser = modelMapper.map(appUserDto, AppUser.class);
        response.setPayload(appUserService.registerAppUser(appUser));
        response.setStatus(true);
        response.getMessage().add("App User Saved");
        return ResponseEntity.ok(response);

    }

    @GetMapping("/{id}")
    public AppUser findOne(@PathVariable("id") Long id){
        return appUserService.findById(id);
    }

    @GetMapping("/register")
    public Iterable<AppUser> findAll(){
        return appUserService.findAll();
    }

    @PostMapping("/{id}")
    public void addEng(@RequestBody Engineer engineer,@PathVariable("id") Long id){
        appUserService.addEngineer(engineer, id);
    }
    

    @PostMapping("/support/{id}")
    public void addSup(@RequestBody Support support, @PathVariable("id") Long id){
        appUserService.addSupport(support, id);
    }


    private static final Logger logger =  LoggerFactory.getLogger(AppUserController.class);

    @GetMapping("/user")
    public ResponseEntity<AppUserDto> getUser(@AuthenticationPrincipal AppUser appUser){
        logger.info("User " + appUser.getEmail() + " " + appUser.getPassword() + " logged in.");

        AppUserDto user = new AppUserDto();
        user.setEmail(appUser.getEmail());
        user.setPassword(appUser.getPassword());

        return ResponseEntity.ok(user);

    }

}
