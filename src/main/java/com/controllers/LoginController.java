package com.controllers;

import com.dto.SearchFormData;
import com.entities.ListReport;
import com.services.ListReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class LoginController {


    @Autowired
    private ListReportService listReportService;

    @GetMapping
    public String welcome(Model model){

        
        String messages ="TEST SPRING MVC";
        model.addAttribute("msg", messages);
        model.addAttribute("searchForm", new SearchFormData());
        model.addAttribute("listReports", listReportService.findAll());
        return "index";
    }

    @GetMapping("/form")
    public String form(Model model){
        model.addAttribute("listreport", new ListReport());
        return "form";
    }   

    @PostMapping("/save")
    public String save(ListReport listReport, Model model){
        System.out.println(listReport.getDescription());
        listReportService.create(listReport);
        // model.addAttribute("listReports", listReportService.findAll());
        return "redirect:/";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id){
        listReportService.deleteById(id);
        return "redirect:/";
        
    }


    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model){
        model.addAttribute("listreport", listReportService.findOne(id));
        return "edit";
    }

    @PostMapping("/update")
    public String update(ListReport listReport, Model model){
        
        // listReportService.create(listReport);
        model.addAttribute("listReports", listReportService.create(listReport));
        return "redirect:/";
    }

    @PostMapping("/search")
    public String search(SearchFormData searchFormData, Model model){

        model.addAttribute("searchForm", searchFormData);
        model.addAttribute("listReports", listReportService.findByTitle(searchFormData.getKeyword()));
        return "index";

    }

    @GetMapping("/api/login")
    public String login() {
        return "login2";
      }

      @GetMapping("/home")
    public String home() {
        return "redirect:/";
      }

      @GetMapping("/logout")
      public String logout(){
          return "login2";
      }

//       @GetMapping("/api/error")
//     public String loginError(Model model) {
//     model.addAttribute("loginError", true);
//     return "login.html";
//   }
}
