package com.controllers;

import javax.validation.Valid;

import com.dto.ResponseData;
import com.entities.ReportSupport;
import com.entities.Support;
import com.services.SupportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/support")
public class SupportController {
    
    @Autowired
    private SupportService supportService;


    // @PostMapping
    // public Support save(Support support){
    //     return supportService.create(support);
    // }

    @PostMapping
    public ResponseEntity<ResponseData<Support>> save(@Valid @RequestBody Support support, Errors errors ){
        ResponseData responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for(ObjectError erro : errors.getAllErrors()){
                responseData.getMessage().add(erro.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.getMessage().add("success");
        responseData.setPayload(supportService.create(support));
        return ResponseEntity.ok(responseData);
    }


    @GetMapping("/{id}")
    public Support finOne(@PathVariable("id") Long id ){
        return supportService.findOne(id);
    }

    
    @GetMapping
    public Iterable<Support> findAll(){
        return supportService.findAll();
    }

    //join
    @PostMapping("/{id}")
    public void addSupport(@RequestBody ReportSupport reportSupport,@PathVariable("id") Long id){
        supportService.addReport(reportSupport, id);
    }
}
