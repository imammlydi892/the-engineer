package com.controllers;

import java.util.UUID;

import javax.validation.Valid;

import com.dto.EngineerDto;
import com.dto.ResponseData;
import com.entities.Engineer;
import com.entities.Support;
import com.repo.EngineerRepo;
import com.services.EngineerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("api/engineer")
public class EngineerController {
    
    @Autowired
    private EngineerService engineerService;

    // @Autowired
    // private Support support;

    // @Autowired
    // private EngineerRepo engineerRepo;

    
    @GetMapping
    public Iterable<Engineer> findAll(){
        return engineerService.findAll();
    }

    @GetMapping("/{id}")
    public Engineer findOne(@PathVariable("id") Long id){
        return engineerService.finOne(id);
    }
    
    @PostMapping
    public ResponseEntity<ResponseData<Engineer>> create (@Valid @RequestBody Engineer engineer, Errors errors){

        ResponseData responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for(ObjectError erro : errors.getAllErrors()){
                responseData.getMessage().add(erro.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        responseData.setStatus(true);
        responseData.setPayload(engineerService.create(engineer));
        responseData.getMessage().add("success");
        return ResponseEntity.ok(responseData);
    }

    @PutMapping
    public ResponseEntity<ResponseData<Engineer>> update (@Valid @RequestBody Engineer engineer, Errors errors){

        ResponseData responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for(ObjectError erro : errors.getAllErrors()){
                responseData.getMessage().add(erro.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        responseData.setStatus(true);
        responseData.setPayload(engineerService.create(engineer));
        responseData.getMessage().add("success");
        return ResponseEntity.ok(responseData);
    }



    @PostMapping("/{id}")
    public void addSupport(@RequestBody Support support, @PathVariable("id") Long id ){
     engineerService.addSupport(support, id);
    }


    // @PostMapping("/saveEng")
    // public Engineer saveEng(@RequestBody EngineerDto engineerDto){
    //     return engineerService.create(engineerDto.getEngineer());
    // }


    // @GetMapping("/{id}")
    // public Engineer find(@PathVariable("id") Long id){
    //     return engineerService.finOne(id);
    // }
}
