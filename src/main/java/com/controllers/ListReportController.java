package com.controllers;

import com.dto.ResponseData;
import com.entities.ListReport;
import com.services.ListReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/listReport")
public class ListReportController {
    
    @Autowired
    private ListReportService listReportService;

    @GetMapping
    public Iterable<ListReport> finAll(){
        return listReportService.findAll();
    }

    @PostMapping
    public ResponseEntity<ResponseData<ListReport>>  save(@RequestBody ListReport listReport, Errors errors){
        ResponseData responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for(ObjectError erro: errors.getAllErrors()){
                responseData.getMessage().add(erro.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(listReportService.create(listReport));
        return ResponseEntity.ok(responseData);
       
    }
}
