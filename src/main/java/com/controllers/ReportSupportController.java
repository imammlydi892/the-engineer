package com.controllers;

import com.dto.ResponseData;
import com.entities.ListReport;
import com.entities.ReportSupport;
import com.entities.Weather;
import com.services.ReportSupportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/reportSupport")
public class ReportSupportController {
    

    @Autowired
    private ReportSupportService reportSupportService;

    @PostMapping
    public ResponseEntity<ResponseData<ReportSupport>> save(@RequestBody ReportSupport reportSupport, Errors errors){
        ResponseData responseData= new ResponseData<>();
        if(errors.hasErrors()){
            for(ObjectError erro : errors.getAllErrors()){
                responseData.getMessage().add(erro.getDefaultMessage());
            }
            responseData.setPayload(null);
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(reportSupportService.create(reportSupport));
        return ResponseEntity.ok(responseData);
    }

    @GetMapping("/{id}")
    public ReportSupport findOne(@PathVariable("id") Long id){
        return reportSupportService.finOne(id);
    }

    @GetMapping
    public Iterable<ReportSupport> findAll(){
        return reportSupportService.findAll();
    }

    @PostMapping("/weather/{id}")
    public void addWeather(@RequestBody Weather weather,@PathVariable("id") Long id){
        reportSupportService.addWeather(weather, id);
    }

    @PostMapping("/list/{id}")
    public void addListReport(@RequestBody ListReport listReport,@PathVariable("id") Long id){
        reportSupportService.addListReport(listReport, id);
    }
}
