package com.controllers;

import com.dto.ResponseData;
import com.entities.Weather;
import com.services.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/weather")
public class WeatherController {
    
    @Autowired
    private WeatherService weatherService;

    @PostMapping
    public ResponseEntity<ResponseData<Weather>> save(@RequestBody Weather weather, Errors errors){

        ResponseData responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for(ObjectError erro : errors.getAllErrors()){
                responseData.getMessage().add(erro.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.getMessage().add("success");
        responseData.setPayload(weatherService.create(weather));
        return ResponseEntity.ok(responseData);
    }



    @GetMapping
    public Iterable<Weather> findAll(){
        return weatherService.findAll();
    }
}
