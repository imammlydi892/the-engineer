package com.repo;

import java.util.UUID;

import com.entities.Weather;

import org.springframework.data.jpa.repository.JpaRepository;



public interface WeatherRepo extends JpaRepository<Weather,Long> {
    
}
