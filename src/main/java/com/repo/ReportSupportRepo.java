package com.repo;

import com.entities.ReportSupport;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportSupportRepo extends JpaRepository<ReportSupport, Long>{
    
}
