package com.repo;



import com.entities.Support;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SupportRepo extends JpaRepository<Support, Long> {
    
}
