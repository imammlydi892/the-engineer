package com.repo;


import java.util.Optional;
import java.util.UUID;

import com.entities.Engineer;

import org.springframework.data.jpa.repository.JpaRepository;


public interface EngineerRepo extends JpaRepository<Engineer, Long>{
 
}  
