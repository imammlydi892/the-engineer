package com.repo;

import java.util.List;

import com.entities.ListReport;

import org.springframework.data.jpa.repository.JpaRepository;



public interface ListReportRepo extends JpaRepository<ListReport, Long> {
    List<ListReport> findByTitleContains(String keyword);
}
