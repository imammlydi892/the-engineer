package com.repo;


import java.util.Optional;

import com.entities.AppUser;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface AppUserRepo extends PagingAndSortingRepository<AppUser,Long> {
    
    //untuk Login
    Optional<AppUser> findByEmail(String email);
}
