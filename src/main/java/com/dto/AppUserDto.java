package com.dto;

import lombok.Data;

@Data
public class AppUserDto {
    
    private String fullName;

    private String email;
    
    private String password;

    private String appUserRole;
}
