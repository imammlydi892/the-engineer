package com.dto;

import com.entities.Engineer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EngineerDto {
    
    private Engineer engineer;
}
