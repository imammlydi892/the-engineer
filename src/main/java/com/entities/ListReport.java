package com.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "tbl_listReport")
public class ListReport implements Serializable {
    
    
    //@GeneratedValue()
    // private UUID uuid= UUID.randomUUID();
    @Id
    @GeneratedValue()
    private Long id;

    @Column(length =200)
    private String title;


    @Column(length =1500)
    private String description;

    @Column(length =7000)
    private String photo;

    // @ManyToOne
    // private ReportSupport reportSupport;

    public ListReport(){
        
    }



}
