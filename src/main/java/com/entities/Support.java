package com.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;


@Entity
@Table(name = "tbl_support")
@Data
public class Support implements Serializable {
    
   
    // private UUID uuid= UUID.randomUUID();
    @Id
    @GeneratedValue()
    private Long id ;

    @Column(length = 300)
    @NotEmpty(message = "Name is Required")
    private String name;

    @Column(length = 500)
    private String address;

    @Column(length = 500)
    @NotEmpty(message = "Dicipline is Required")
    private String dicipline;

    private boolean isActive;


    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "support")
    private AppUser user;

    //@OneToMany(targetEntity = ReportSupport.class, cascade = CascadeType.ALL)
    ///@JoinColumn(name = "reportSupports", referencedColumnName = "uuid")
    //@JsonIgnore
    @OneToMany(targetEntity = ReportSupport.class, cascade = CascadeType.ALL)
    @JoinColumn(name ="reportSupports" )
    //@JsonManagedReference
    private List<ReportSupport> reportSupports ;

  


    // @ManyToOne
    // private Engineer engineer;
    


}
