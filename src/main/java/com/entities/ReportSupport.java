package com.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;


@Entity
@Data
@Table(name = "tbl_reportSupport")
public class ReportSupport extends BaseEntity<String> implements Serializable {

    
    //@GeneratedValue()
    // private UUID uuid= UUID.randomUUID();
    @Id
    @GeneratedValue()
    private Long id;

    @Column(length = 500)
    private String titleReport;

    @Column(length = 1500)
    private String descriptionReport;

    // @OneToMany(targetEntity = Weather.class, cascade = CascadeType.ALL)
    // @JoinColumn(name="weathers", referencedColumnName = "uuid")
    @OneToMany(targetEntity = Weather.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "weathers",referencedColumnName = "id")
    //@JsonManagedReference
    private List<Weather> weathers;


    // @OneToMany(targetEntity = ListReport.class, cascade = CascadeType.ALL)
    // @JoinColumn(name = "lisreports", referencedColumnName = "uuid")
    @OneToMany(targetEntity = ListReport.class, cascade = CascadeType.ALL )
    @JoinColumn(name = "listReport",referencedColumnName = "id")
    ///@JsonManagedReference
    private List<ListReport> listReport;

    // @ManyToOne
    // private Support support;
    

}
