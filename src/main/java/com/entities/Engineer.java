package com.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

// import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "tbl_engineer")
@Data
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id"
)
public class Engineer  implements Serializable {
    
    
    // @GeneratedValue(generator = "uuid" )
    // @GenericGenerator(name="uuid" ,  strategy = "uuid2")
    //@GeneratedValue()
    // private UUID id ;
    @Id
    @GeneratedValue()
    private Long id ;

    @NotEmpty(message = "Name is Required")
    @Column(length=200)
    private String name ;

    @NotEmpty(message = "Email is Required")
    @Column(length=200, unique=true)
    private String email;

    @Column(length=500)
    private String address; 

    @NotEmpty(message = "Dicipline is Required")
    @Column(length=200)
    private String Dicipline;

    private boolean isActive;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "engineer")
    // @JsonManagedReference
    private AppUser user;

    

    // @OneToMany(targetEntity = Support.class, cascade = CascadeType.ALL)
    // @JoinColumn(name = "supports", referencedColumnName = "uuid")
    //@JsonIgnore
    @OneToMany(targetEntity = Support.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "supports")
    //@JsonManagedReference
    private List<Support> supports ;



    // public Engineer(@NotEmpty(message = "Name is Required") String name,
    //         @NotEmpty(message = "Email is Required") String email) {
    //     this.name = user.getFullName();
    //     this.email = user.getEmail();
       
    // }

    
    
}
