package com.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "tbl_Weather")
public class Weather implements Serializable {
    
    
    //@GeneratedValue()
    // private UUID uuid = UUID.randomUUID();
    @Id
    @GeneratedValue()
    private Long id;

    @Column(length = 500)
    private String precipitation;

    @Column(length = 500)
    private String temperature;

    @Column(length = 500)
    private String humadity;
    
    @Column(length = 500)
    private String wind_velocity;


    // @ManyToOne()
    // private ReportSupport reportSupport;
}
